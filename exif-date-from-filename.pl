#!/usr/bin/perl

use Image::ExifTool qw(:Public);
use File::Spec;
use DateTime::Format::Strptime qw(strptime strftime);

my @patterns = (
	"IMG_%Y%m%d_%H%M%S",
	"BURST%Y%m%d%H%M%S",
);

sub process_file {
	$file = $_[0];

	$exifTool = new Image::ExifTool;
	$exifTool->ExtractInfo($file);

	$value = $exifTool->GetValue("ModifyDate");

	if ($value eq "") 
	{
		 $volume,$directories,$filename =
                       File::Spec->splitpath( $file );

		my $date_set = 0;
		foreach my $pattern (@patterns) {
			$strp = DateTime::Format::Strptime->new(
				pattern   => $pattern
			);

			if ($dt = $strp->parse_datetime($filename)) {
				$fmt_dt = strftime("%Y:%m:%d %H:%M:%S", $dt);
				$exifTool->SetNewValue("ModifyDate", $fmt_dt);
				$exifTool->WriteInfo($file);
				print "Updated $file to have EXIF ModifyDate $fmt_dt\n";
				$date_set = 1;
			}
			last if ($date_set);

		} 
		if (!$date_set) {
			warn "Error parsing filename $file, file untouched";
		}
	} else {
		print "Ignoring $file as ModifyDate already set.\n";
	}
}

foreach $file (@ARGV) {
	process_file($file);
}
