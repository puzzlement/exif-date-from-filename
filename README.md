Set EXIF date from filename
===========================

A script that uses the date embedded in filenames like IMG\_20160103\_162625.jpg
(IMG\_YYYMMDD\_HHMMSS.jpg) to set the EXIF date data.

This can be used to add data missing after [this
bug](https://www.reddit.com/r/nexus6/comments/3wh31p/exif_datetime_created_info_is_missing_from_normal/)
in the Nexus 6 phone where EXIF dates were not added to non-HDR photos.

Requirements
============

* Perl 5 (tested with Perl 5.22.1)
* `Image::ExifTool`
* `File::Spec`
* `DateTime::Format::Strptime`

Usage
=====

    exif-date-from-filename.pl [image filenames…]

Image filenames should be exactly of the form IMG\_YYYMMDD\_HHMMSS.jpg

**Note**: Set EXIF date from filename is *destructive*, ie, it overwrites the
input file. make a copy of your input files before running it.

About
=====

Credits
-------

Set EXIF date from filename was developed by [Mary
Gardiner](https://mary.gardiner.id.au/).

Licence
-------

Set EXIF date from filename is free software available under the MIT licence.
See LICENCE for full details.
